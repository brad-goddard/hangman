// Vars
let maxCount = 7;
let count = 0;
let countDown = maxCount;
let valid;
let hangmanWord = null;
let letterArray = [];

function countReset() {
	countDown = maxCount;
	count = 0;
}
/*
Generate the word.
 */
function genWord() {
	$('#count-wrap p span').html(maxCount);
	// Adds disable to letter that is already chosen.
	$('#alphabet li').each(function () {
		$(this).find('button').removeClass('disable');
	});
	$('#alphabet li button').removeClass('disbale');
	// Ajax request to get the word.
	$.ajax({
		url: './ajax/hangmanWord.php',
		async: false,
		method: 'POST',
		data: {
			start: true,
		},
		success: function (data) {
			hangmanWord = data.data.word;
		}
	});
	// Ajax request to output the word.
	$('#hangman-word-wrap').fadeTo(0, 0, function () {
		$('#hangman-word-wrap').fadeTo(0, 1);
		$.ajax({
			url: './ajax/drawWord.php',
			method: 'POST',
			data: {
				word: hangmanWord,
			},
			success: function (data) {
				$('#hangman-word').replaceWith(data);
			}
		});
	});
}

function changeImage(number) {
	$.ajax({
		url: './ajax/updateImage.php',
		method: 'POST',
		data: {
			image: number,
		},
		success: function (data) {
			$('#hangman-icon .image-wrap').replaceWith(data);
		}
	});
}

function setTimerFadeOut(div) {
	setTimeout(function () {
		$(div).removeClass('open');
	}, 1600);
}

/*
Loser function.
 */
function gameOver() {
	$('#loser').addClass('open');
	setTimerFadeOut('#loser');
}

/*
Winner function.
 */
function winner() {
	$('#winner').addClass('open');
	setTimerFadeOut('#winner');
}

$(document).ready(function () {
	/*
Generate the word button.
 */
	$('body').on('click', '#generate-word', function (e) {
		e.preventDefault();
		countReset();
		genWord();
	});

	/*
	Letter click
	 */
	$('body').on('click', '#alphabet button', function (e) {
		e.preventDefault();
		letterArray = hangmanWord.split('');
		let letter = $(this).attr('data-letter');
		count++;
		countDown--;
		// If a letter is correct then don't change the count and countDown.
		$(letterArray).each(function (key, value) {
			value = value.toLowerCase();
			if (value == letter) {
				count--;
				countDown++;
				return false;
			}
		});
		// If count goes over maxCount then stop.
		if (count == maxCount) {
			countDown = maxCount;
			valid = false;
			$('#count-wrap p span').html(countDown);
			gameOver();
			changeImage(maxCount);
		} else {
			valid = true;
		}
		if (valid == true) {
			if (!$(this).hasClass('disbale')) {
				$(this).addClass('disable');
				$('#count-wrap p span').html(countDown);
				$(letterArray).each(function (key, value) {
					value = value.toLowerCase();
					if (value == letter) {
						$('#hangman-word .span-wrap #' + key).addClass('correct').html(value);
					}
					if (value != letter) {
						changeImage(count);
					}
				});
				// If all letters are selected in the word.
				if ($("#hangman-word .span-wrap .letter.correct").length == $("#hangman-word .span-wrap .letter").length) {
					winner();
				}
			}
		}
	});
});
