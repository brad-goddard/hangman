<?php
/**
 * Sets up the view that you have set for the matching URL.
 * Class Route
 *
 * @package bradGoddard
 */
class Route {

	public static $validRoutes = [];

	/**
	 * @param $route
	 * @param $function
	 * If URL matches a route than output the function.
	 */
	public static function set( $route, $function ) {
		self::$validRoutes[] = $route;

		if ( isset($_GET['url']) && $_GET['url'] == $route ) {
			$function->__invoke();
		}
	}

	/**
	 * @return mixed|string
	 * Returns the route of the page you are on.
	 */
	public static function currentRoute() {
		if (isset($_GET['url']) && $_GET['url'] === 'index.php') {
			$currentRoute = 'hangman';
		} else {
			$currentRoute = $_GET['url'];
		}
		return $currentRoute;
	}

	public static function getRouteName() {
		if (isset($_GET['url']) && $_GET['url'] === 'index.php') {
			$routeName = 'hangman';
		} else {
			$routeName = $_GET['url'];
			$routeName = str_replace('-', ' ', $routeName);
			$routeName = ucwords($routeName);
			$routeName = str_replace(' ', '', $routeName);
			$routeName = lcfirst($routeName);
		}
		return $routeName;
	}
}
