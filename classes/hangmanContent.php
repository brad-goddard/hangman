<?php
/**
 * Class for hangmanConent
 *
 * @package bradGoddard
 */

class HangmanContent {
	private $maxGoes;

	public function __construct() {
		$this->maxGoes = '7';
	}

	/**
	 * Draws alphabet
	 */
	public function drawAlphabet() {
		$alphas = range( 'A', 'Z' );
		echo "
		<div id=\"alphabet\">
			<ul>";
		foreach ( $alphas as $row ) {
			$slug = strtolower( $row );
			echo "
				<li><button type=\"submit\" id=\"letter-{$slug}\" class=\"disable\" data-letter=\"{$slug}\">{$row}</button></li>
				";
		}
		echo "
			</ul>
		</div>
		";
	}

	/**
	 * Draws the word.
	 *
	 * @param $word
	 */
	public function drawWord( $word ) {
		// Split each letter in to an array.
		$wordArray = str_split( $word );
		echo "
		<div id=\"hangman-word\">
			<p>";
		$i = 0;
		foreach ( $wordArray as $row ) {
			$slug = strtolower( $row );
			echo "<span class=\"span-wrap\"><span class=\"letter\" id=\"{$i}\">&nbsp;</span></span>";
			$i ++;
		}
		echo "
			</p>";
		$this->drawInput( 'hidden-word', 'hidden', $word );
		echo "</div>";
	}

	/**
	 * Gets the chosen word.
	 * @return mixed
	 */
	public function getWord() {
		$words = $this->getWords();
		// Gets random word from array.
		$word = $words[ array_rand( $words ) ];

		return $word;
	}

	/**
	 * Draws a button
	 *
	 * @param $id
	 * @param $value
	 */
	public function drawButton( $id, $value ) {
		echo "<button id=\"{$id}\">{$value}</button>";
	}

	/**
	 * Draws the hangman title.
	 */
	public function drawBlankWord() {
		echo "<p>
				<span class=\"span-wrap\"><span>H</span></span>
				<span class=\"span-wrap\"><span>a</span></span>
				<span class=\"span-wrap\"><span>n</span></span>
				<span class=\"span-wrap\"><span>g</span></span>
				<span class=\"span-wrap\"><span>m</span></span>
				<span class=\"span-wrap\"><span>a</span></span>
				<span class=\"span-wrap\"><span>n</span></span>
			</p>";
	}

	/**
	 * Draws a basic input.
	 *
	 * @param $id
	 * @param $type
	 * @param $value
	 */
	public function drawInput( $id, $type, $value ) {
		echo "<input id=\"{$id}\" type=\"{$type}\" value=\"{$value}\">";
	}

	/**
	 * Draws the count number.
	 */
	public function drawCount() {
		echo "
		<div id=\"count-wrap\">
			<p>Guesses left <span>{$this->maxGoes}</span></p>
		</div>
		";
	}

	/**
	 * Returns array of each the words in the hangman game.
	 * @return array
	 */
	private function getWords() {
		return $words = [
			'And',
			'Cat',
			'Ran',
			'Sofa',
			'Pole',
			'Pillow',
			'Chair',
			'Drive',
			'Remote',
			'Raining',
			'Building',
			'Pavement',
		];
	}

	public function outputHangman() {
		$hangedMan = [
			$this->getImage('1.jpg'),
			$this->getImage('2.jpg'),
			$this->getImage('3.jpg'),
			$this->getImage('4.jpg'),
			$this->getImage('5.jpg'),
			$this->getImage('6.jpg'),
			$this->getImage('7.jpg'),
			$this->getImage('8.jpg'),
		];

		return $hangedMan;
	}

	private function getImage($imgName) {
		return "<div class=\"image-wrap\"><img src=\"./public/assets/images/steps/{$imgName}\"></div>";
	}
}
