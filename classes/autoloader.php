<?php
/**
 * Class Autoloader
 */
class Autoloader {
	/**
	 * @param $className
	 *
	 * @return bool
	 */
	static public function loader($className) {
		$className = lcfirst($className);
		if ( file_exists( './classes/' . $className . '.php' ) ) {
			require_once './classes/' . $className . '.php';
		} else if ( file_exists( './controllers/' . $className . '.php' ) ) {
			require_once './controllers/' . $className . '.php';
		}
		return true;
	}
}
