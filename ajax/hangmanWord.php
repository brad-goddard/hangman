<?php
/**
 * Hangman word ajax request
 *
 * @package bradGoddard
 */

if ( isset( $_POST['start'] ) ) {
	$hangman = new HangmanContent();
	$word = $hangman->getWord();
	header( 'Content-Type: application/json' );
	http_response_code( 200 );
	exit( json_encode( [
		'success' => false,
		'message' => 'success',
		'data'    => [
			'word' => $word,
		]
	] ) );
}
