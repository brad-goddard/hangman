<?php
/**
 * Hangman word ajax request
 *
 * @package bradGoddard
 */

if ( isset( $_POST['word'] ) ) {
	$hangman = new HangmanContent();
	$word = $_POST['word'];
	$hangman->drawWord($word);
}
