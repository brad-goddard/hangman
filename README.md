**Brad Goddard's Hangman**
---
##Using MAMP##
1. Go to command line and type **sudo nano /etc/hosts**
2. Type in your password
3. Add a virtual host record for **127.0.0.1 hangman.test**
4. Press **control o** then **control x** to save the virtual host.
5. Go to /Applications/MAMP/conf/apache/extra/httpd-vhosts.conf and open the httpd-vhosts.conf file.
6. Add these records 
`<VirtualHost *:80>
DocumentRoot /Applications/MAMP/htdocs/hangman
ServerName hangman.test
</VirtualHost>`
6. Restart mamp and go hangman.test in your browser. If this doesn't load then try clearing your cache.
7. Have fun!
