<?php
/**
 * Add pages here.
 *
 * @package bradGoddard
 */

// This is the index page. The first route.
Route::set('index.php', function() {
	Index::createView('hangman');
});
