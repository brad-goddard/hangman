<?php
/**
 * View for hangman
 *
 * @package bradGoddard
 */

$hangman = new HangmanContent();
$hangedMan = $hangman->outputHangman();
?>
<section class="hangman-section">
	<div class="container clearfix">
		<div id="hangman-wrap" class="grid-wrap row-2">
			<div id="hangman-word-wrap" class="grid-item">
				<div id="hangman-word"><?php $hangman->drawBlankWord(); ?></div>
			</div><!--END hangman-word-wrap-->
			<div id="hangman-icon" class="grid-item">
				<?php echo $hangedMan[0]; ?>
			</div><!--END hangman-icon-->
		</div><!--END hangman-wrap-->
		<div id="hangman-buttons">
			<?php
			$hangman->drawCount();
			$hangman->drawButton('generate-word', 'Generate a word');
			?>
		</div><!--END hangman-buttons-->
		<?php
		$hangman->drawInput('count', 'hidden', '0');
		$hangman->drawAlphabet();
		?>
	</div><!--END container-->
	<div id="winner" class="end-image cover" style="background-image: url('./public/assets/images/winner.jpg');">
		<h2>Winner</h2>
	</div><!--END winner-->
	<div id="loser" class="end-image cover" style="background-image: url('./public/assets/images/loser.jpg');">
		<h2>Loser</h2>
	</div><!--END loser-->
</section><!--END hangman section-->
