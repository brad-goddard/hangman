<?php
/**
 * Hangman footer include.
 *
 * @package bradGoddard
 */

?>
</div><!--END container-->
</div><!--END main-content-->
<script src="./public/assets/js/scripts.js<?php cacheBuster::VERSION; ?>" type="module"></script>
<script src="https://code.jquery.com/jquery-3.5.1.min.js" integrity="sha256-9/aliU8dGd2tb6OSsuzixeV4y/faTqgFtohetphbbj0=" crossorigin="anonymous"></script>
</body>
</html>
