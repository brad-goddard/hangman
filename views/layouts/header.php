<?php
/**
 * Hangman header include
 *
 * @package bradGoddard
 */

// Gets Route name.
$routeName = new Route;
$routeName = $routeName->getRouteName();
// Creates class using the route name to the current views route.
$route = new $routeName;
?>
<!DOCTYPE html>
<html lang="en">
<head>
	<title><?php echo $route->pageMetaTitle(); ?></title>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<!-- Favicon -->
	<link rel="icon" href="./public/assets/img/brand/favicon.png" type="image/png">
	<link rel="icon" href="./public/assets/img/brand/01164414/cropped-favicon1-32x32.png" sizes="32x32"/>
	<link rel="icon" href="./public/assets/img/brand/cropped-favicon1-192x192.png" sizes="192x192"/>
	<!--Fonts-->
	<link rel="preconnect" href="https://fonts.gstatic.com">
	<link href="https://fonts.googleapis.com/css2?family=Lato:wght@400;700&display=swap" rel="stylesheet">
	<!-- Icons -->
	<script src="https://kit.fontawesome.com/e8b6bd7bcd.js" crossorigin="anonymous"></script>
	<!-- Argon CSS -->
	<link type="text/css" href="./public/assets/css/styles.css<?php cacheBuster::VERSION; ?>" rel="stylesheet" type="text/css">
</head>
<body class="g-sidenav-hidden <?php echo Route::currentRoute(); ?>">
<?php
