<?php
/**
 * Class HangmanContent
 *
 * @package bradGoddard
 */

class Hangman extends MainController {
	/**
	 * @return string
	 * Page meta title.
	 */
	public static function pageMetaTitle() {
		return 'Brad Goddard\'s Hangman';
	}
}
