<?php
/**
 *  Main controller.
 *
 * @package bradGoddard
 */

class MainController {

	/**
	 * @param $viewName
	 *
	 * Matches the view to the route.
	 */
	public static function createView( $viewName ) {
		require_once( 'views/layouts/header.php' );
		require_once( './views/' . $viewName . '.php' );
		require_once( 'views/layouts/footer.php' );
	}
}
