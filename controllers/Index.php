<?php
/**
 *  Index page controller.
 *
 * @package bradGoddard
 */

class Index extends MainController {

	/**
	 * @return string
	 * Page meta title.
	 */
	public static function pageMetaTitle() {
		return 'Brad Goddard\'s Hangman';
	}
}
