<?php
/**
 * Index file for Brad Goddards Hangman.
 *
 * @package bradGoddard
 */

// Errors on.
ini_set( 'error_reporting', E_ALL );
ini_set( 'display_errors', 'On' );
// Enums.
require_once( 'config/enums.php' );
// Autoloader class.
require_once( 'classes/autoloader.php' );
// Register autoloader.
spl_autoload_register('Autoloader::loader');
// Ajax.
require_once( 'ajax/hangmanWord.php' );
require_once( 'ajax/drawWord.php' );
require_once( 'ajax/updateImage.php' );
// Routes.
require_once( 'routes.php' );
